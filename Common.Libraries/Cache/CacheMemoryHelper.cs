﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;

namespace Common.Libraries
{
    /// <summary>
    /// “Cache Memory”
    /// 缓存。
    /// </summary>
    public class CacheMemoryHelper
    {
        /* Cache Memory 跟 Redis 有什么区别？？
         * # 都是内存缓存，但是“redis”可以持久化，“memory cache”不可持久化，存储数据大小不能超过内存大小；
         * # “redis”支持多种数据结构，而“memory cache”只支持string类型；
         * # “memory cache”不可分布式读取，内存不可跨机器；
         * # “redis”单线程，“memory cache”多线程；
         */

        /// <summary> 无参构造。 </summary>
        public CacheMemoryHelper() { }

        /// <summary>
        /// “MemoryCache.Default”底层实现本身就是单例模式，无须再次去进行重复单例；
        /// </summary>
        protected ObjectCache Cache => MemoryCache.Default;

        /// <summary><see cref="T:System.Int32" /> 已缓存个数</summary>
        public int Count => (int)(Cache.GetCount());

        /// <summary>set/get</summary>
        /// <param name="key">该缓存项的唯一标识符。</param>
        /// <returns><see cref="T:System.Object" /> “Key”对应的缓存的值。</returns>
        public object this[string key] { get { return Cache.Get(key); } set { Add(key, value); } }

        /// <summary>Get</summary>
        /// <typeparam name="T">形参: 类型参数</typeparam>
        /// <param name="key">该缓存项的唯一标识符。</param>
        /// <returns><see cref="T:T" /> “Key”对应的缓存的值。</returns>
        public T Get<T>(string key) { if (Cache.Contains(key)) return (T)Cache[key]; else return default(T); }

        /// <summary>Get</summary>
        /// <param name="key">该缓存项的唯一标识符。</param>
        /// <returns><see cref="T:System.Object" /> “Key”对应的缓存的值。</returns>
        public object Get(string key) => Cache[key];

        /// <summary>Add 过期时间: 为null则不过期。</summary> 
        /// <param name="key">该缓存项的唯一标识符。</param>
        /// <param name="value">value</param>
        /// <param name="cacheTime">过期时间: 为null则不过期。</param>
        public void Add(string key, object value, int? cacheTime = null)
        {
            if (value == null) return;

            /* 过期策略：
             * 永久有效：永不过期；
             * 绝对过期：设置一个时间点，超过时间点就过期；
             * 滑动过期：多久之后过期，如果期间查询/更新，就再次延长过期时间；
             */
            var policy = new CacheItemPolicy();
            if (cacheTime != null) policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(Convert.ToDouble(cacheTime));
            Cache.Add(new CacheItem(key, value), policy);
        }

        /// <summary>Add 过期时间: 为null则不过期。</summary> 
        /// <param name="key">该缓存项的唯一标识符。</param>
        /// <param name="value">value</param>
        /// <param name="cacheTime">过期时间: 为null则不过期。</param>
        public void Add(string key, object value, string regionName, int? cacheTime = null)
        {
            if (value == null) return;
            var policy = new CacheItemPolicy();
            if (cacheTime != null) policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(Convert.ToDouble(cacheTime));
            Cache.Add(new CacheItem(key, value, regionName), policy);
        }

        /// <summary> 是否存在 </summary>
        /// <param name="key"></param>
        /// <returns><see cref="T:System.Boolean" /> Key是否存在值。</returns>
        public bool Contains(string key) => Cache.Contains(key);

        /// <summary> Remove All </summary>
        public void RemoveAll() { foreach (var item in Cache) Remove(item.Key); }

        /// <summary> Remove </summary>
        /// <param name="key">该缓存项的唯一标识符。</param>
        public void Remove(string key) => Cache.Remove(key);

        /// <summary> Remove </summary>
        /// <param name="keys">Key集合,该缓存项的唯一标识符。</param>
        public void Remove(List<string> keys) => keys.ForEach(key => { Cache.Remove(key); });
    }
}
