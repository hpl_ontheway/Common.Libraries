﻿using System;
using System.Threading;

namespace Common.Libraries.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            CacheMemoryHelper helper = new CacheMemoryHelper();
            Console.WriteLine("Not RegionName ****");
            string key = "key_hsl";
            string regionName = "key_regionName";
            //helper.Add(key, "is Not RegionName", regionName);
            //set
            Console.WriteLine("set: is Not RegionName");
            //Thread.Sleep(1000 * 20);
            //get
            Console.WriteLine($"get : {helper.Get<string>(key, regionName)}");

            Console.Read();
        }
    }
}
